package com.example.tema2.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.tema2.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_go_to_session.setOnClickListener{
            goToSessionActivity()
        }
    }

    private fun goToSessionActivity(){
        val intent= Intent(this, FirstActivity::class.java)
        startActivity(intent)
    }

}
