package com.example.tema2.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tema2.R
import com.example.tema2.models.User
import kotlinx.android.synthetic.main.item_user.view.*

class UsersListAdapter(
    private val usersList:ArrayList<User>,
    private var onClick: ((User) -> Unit)? = null
): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int = usersList.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater  = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.item_user, parent, false)
        return UserViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        var user= usersList[position]
        //holder.bind(user,position)
        when(holder){
            is UserViewHolder -> holder.bind(user)
        }
    }


    inner class UserViewHolder(private val view: View): RecyclerView.ViewHolder(view){
        fun bind(user: User){
            val userName="${user.firstName} ${user.lastName}"
            view.tv_user_name.text= userName

            view.setOnClickListener {
                onClick?.invoke(user)
            }
        }
    }
}

