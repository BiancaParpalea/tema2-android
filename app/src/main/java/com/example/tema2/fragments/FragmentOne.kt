package com.example.tema2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.tema2.R
import com.example.tema2.adapters.UsersListAdapter
import com.example.tema2.models.User
import kotlinx.android.synthetic.main.fragment_one.*
import kotlinx.android.synthetic.main.fragment_one.view.*
import org.json.JSONArray
import org.json.JSONObject

class FragmentOne : Fragment() {

    val usersList=ArrayList<User>()
    var usersAdapter: UsersListAdapter? =null

    companion object{
        fun newInstance() = FragmentOne()

        const val ARG_NAME="name"
        const val ARG_USER_NAME="username"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_one, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_sync_with_server.setOnClickListener{
            syncWithServer()
        }

        btn_add_user.setOnClickListener{
            setupUsersList()
        }

        btn_remove_user.setOnClickListener{
            removeFromUserList()
        }
    }


    private fun syncWithServer() {
        setupUsersList()

        val queue = Volley.newRequestQueue(context)
        val url = "https://jsonplaceholder.typicode.com/users"

        val getCommentsRequest = StringRequest(
            Request.Method.GET,
            url,
            Response.Listener { response ->
                handleUserResponse(response)
               // str_comments.isRefreshing = false
            },
            Response.ErrorListener { error ->
                Toast.makeText(
                    context,
                    "Error: get comments failed wit error: ${error.message}",
                    Toast.LENGTH_SHORT
                ).show()

              //  str_comments.isRefreshing = false
            }
        )

        queue.add(getCommentsRequest)
    }

    private fun handleUserResponse(response: String)
    {
        val userJsonArray= JSONArray(response)

        for(index in 0 until userJsonArray.length()) {
            val userJson: JSONObject =userJsonArray[index] as JSONObject
            userJson?.let {
                val firstName= userJson.getString(ARG_NAME)
                val lastName= userJson.getString(ARG_USER_NAME)

                val user=User(
                    firstName = firstName,
                    lastName = lastName
                )

                usersList.add(user)
            }
        }
        Toast.makeText(this.context, "Sync done!", Toast.LENGTH_SHORT).show()
        usersAdapter?.notifyDataSetChanged()
    }

    private fun setupUsersList(){
        val layoutManager= LinearLayoutManager(context)

        if(view?.ed_FirstName?.text.toString().isNotEmpty() && view?.ed_LastName?.text.toString().isNotEmpty()) {
            usersList.add(
                User(
                    view?.ed_FirstName?.text.toString(),
                    view?.ed_LastName?.text.toString()
                )
            )
            Toast.makeText(this.context, "User added!", Toast.LENGTH_SHORT).show()
            ed_FirstName.text.clear()
            ed_LastName.text.clear()
        }

        usersAdapter=UsersListAdapter(usersList){ user ->
            val message=  when(user) {
                is User -> "$user"
                else ->"No user available"
            }
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
        }

        rv_users_list.layoutManager=layoutManager
        rv_users_list.adapter=usersAdapter
    }

    private fun removeFromUserList(){

        for(user in usersList){
            if(user.firstName==ed_FirstName.text.toString() &&
                    user.lastName==ed_LastName.text.toString()){
                usersList.remove(user)
                return
            }
        }

        ed_FirstName.text.clear()
        ed_LastName.text.clear()
        Toast.makeText(this.context, "User deleted!", Toast.LENGTH_SHORT).show()
        setupUsersList()
    }
}

