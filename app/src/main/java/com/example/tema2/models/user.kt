package com.example.tema2.models

open class User (
    open var firstName: String,
    open var lastName: String
)
